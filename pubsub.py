"""
__author__ = 'abulaybulay'
__about__ = 'Pub/sub automation using AWS SQS'
"""

import base64
import boto
import boto.sqs
import boto.sqs.queue
import json
import re
import time
import pyDes

import emailer
import mysql_connect

from boto.sqs.message import Message
from configobj import ConfigObj


config = ConfigObj('config.ini')
#Obtain an AWS access key and secret
conn = boto.sqs.connect_to_region(config['vtw_sqs']['region'], aws_access_key_id=config['vtw_sqs']['aws_access_key_id'],
                                  aws_secret_access_key=config['vtw_sqs']['aws_secret_access_key'], is_secure=True,
                                  validate_certs=False)


def decrypt(data, key):
    decrypter = pyDes.des(key, mode=pyDes.CBC, IV="\0\1\2\3\4\5\6\7")
    decrypted_string = decrypter.decrypt(base64.b64decode(data), padmode=pyDes.PAD_PKCS5)
    return decrypted_string


cursor, con = mysql_connect.db_connect(config['db_credentials']['host'], config['db_credentials']['name'],
               decrypt(config['db_credentials']['username'], 'DESCRYPT'),
               decrypt(config['db_credentials']['password'], 'DESCRYPT'))


def insert_pii(pii, sqs_message):
    print('Inserting pii and message to vtw database')
    cursor.execute("INSERT INTO vtw_24hr_pii SET pii='" + str(pii) + "', date_added=NOW(), sqs_message='" + str(sqs_message) + "'")
    con.commit()
    print 'Done'
    return True


def connect_sqs(url):
    print('Checking for que on AWS SQS')
    que = boto.sqs.queue.Queue(conn, url)
    try:
        print('Checking for connection')
        message = que.read()
        print('Message found %s' % message)
        return 1, message
    except Exception as msg:
        print(msg)
        print('No connection could be made to SQS URL')
        return 0, None


def get_message():
    success, message = connect_sqs(url=config['vtw_sqs']['url'])
    pii_xpath = config['vtw_sqs']['pii_xpath']
    exclude_issn = config['vtw_sqs']['exclude_issn']
    if message:
        try:
            print 'Retrieving PII from message body'
            body = message.get_body()
            print body
            sqs_message = json.loads(body)
            try:
                pii_url = sqs_message["msg:service"]["svc:about"]
                pii = re.search(pii_xpath, pii_url, re.IGNORECASE)
                if pii:
                    pii = pii.group(1)
                    print 'Found PII: ' + pii
                else:
                    print 'PII not found'
                    return None

                sqs = json.dumps(sqs_message)
                print "Checking if " + pii + " is already in the database."
                a=cursor.execute("SELECT pii FROM vtw_24hr_pii WHERE pii='" + pii + "'")
                print a
                if str(a) == '0':
                    if insert_pii(pii, sqs):
                        print 'inserted'
            
            except Exception as msg:
                if pii[:9] not in exclude_issn:
                    print 'Not able to insert pii to database'
                    cursor.execute("SELECT pii FROM vtw_24hr_pii WHERE pii='" + pii + "'")
                    print "Checking if " + pii + " is already in the database."
                    for row in cursor.fetchone()['pii']:
                        done = row['pii']
                    if done:
                        print pii + " is already in the database.  Deleting corresponding PII in SQS."
                        mail = emailer.SendEmail()
                        mail.send_mail(subject=pii + " is already in the database",
                                       message=pii + " is already in the database.  Deleting corresponding PII in SQS.")
                    cursor.execute("INSERT INTO vtw_24hr_error_log (pii, error, datetime, source) VALUES (%s, %s, NOW(), %s)", (pii, msg, 'SQS'))
                    con.commit()
        except Exception as msg:
            print 'No message found available'
            

if __name__ == "__main__":
    while 1:
        get_message()
        print 'Waiting for new message'
        time.sleep(30)
    #insert_pii(pii='123456789', sqs_message='Test')